
import { expect } from 'chai';
import { config } from 'src/config';
import BeablePage from 'src/pageObjects/beable';
import FinalLoginPage from 'src/pageObjects/finalLogin';
import InitialLoginPage from 'src/pageObjects/initialLogin';
import LoginPage from 'src/pageObjects/login';

/**
 * When you specify beable base url in config then please remove 'skip' in describe
 */
describe.skip(`AC1: Testing Beable authorization`, () => {
    it(`AC1.1: Active user can login from home page`, () => {
        BeablePage.getStartedBtn.click();
        InitialLoginPage.assertIsNavigatedToPagePath();
        InitialLoginPage.auth(`New Jersey`, `New Jersey DEMO DISTRICT`);
        FinalLoginPage.assertIsNavigatedToPagePath();
        FinalLoginPage.auth(`sara.smith`, `123456`);
    });

});

describe(`AC1: Test sample login`, () => {
    it('AC1.1: Active user can login', () => {
        LoginPage.navigateTo();
        LoginPage.loginWithCredentials('tomsmith', 'SuperSecretPassword!');

        expect(LoginPage.getErrorMessage()).to.include('You logged into a secure area!');
    });

    it('AC1.1: Active user can login using config values specified in src/config', () => {
        LoginPage.navigateTo();
        LoginPage.loginWithCredentials(config.username, config.password);

        expect(LoginPage.getErrorMessage()).to.include('You logged into a secure area!');
    });

    it('AC1.2: Inactive user can’t login', () => {
        LoginPage.navigateTo();
        LoginPage.loginWithCredentials('foo', 'bar');

        expect(LoginPage.getErrorMessage()).to.include('Your username is invalid!');
    });

});
