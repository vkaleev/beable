
import BasePage from 'src/pageObjects/base';

/**
 * Initial login page object
 */
class InitialLoginPage extends BasePage {
  constructor() {
    super(`/initial-login`);
  }

  get schoolStateInput(): WebdriverIO.Element {
    return $(`#school-state-select`);
  }

  get districtInput(): WebdriverIO.Element {
    return $(`#school-district-select`);
  }

  get letsGoBtn(): WebdriverIO.Element {
    return $(`button=LET'S GO!`);
  }

  /**
   * Fill initial login form and authorize
   */
  public auth(schoolState: string, schoolDistrict: string): void {
    this.schoolStateInput.setValue(schoolState);
    this.districtInput.setValue(schoolDistrict);
    this.letsGoBtn.click();
  }
}

export default new InitialLoginPage();
