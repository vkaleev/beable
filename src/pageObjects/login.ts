
import BasePage from 'src/pageObjects/base';

/**
 * Login page object
 */
class LoginPage extends BasePage {
    constructor() {
        super('/login');
    }

    get usernameInput(): WebdriverIO.Element {
        return $('#username');
    }

    get passwordInput(): WebdriverIO.Element {
        return $('#password');
    }

    get submitBtn(): WebdriverIO.Element {
        return $('#login > button');
    }

    get errorMsgElement(): WebdriverIO.Element {
        return $('#flash');
    }

    getErrorMessage(): string {
        return this.errorMsgElement.getText();
    }

    /**
     * Login on page with username and password
     */
    loginWithCredentials(username: string, password: string): void {
        this.usernameInput.setValue(username);
        this.passwordInput.setValue(password);
        this.submitBtn.click();
    }
}

export default new LoginPage();
