
import { assert } from 'chai';

/**
 * Base class for Page objects
 */
export default class BasePage {
  constructor(protected readonly path: string) {}

  /**
   * Navigate to page
   */
  public navigateTo(): void {
    browser.url(this.path);
  }

  /**
   * Check that browser is navigated to page path
   */
  public assertIsNavigatedToPagePath(): void {
    assert.equal(new URL(browser.getUrl()).pathname, this.path, `Browser is not navigated to path '${this.path}'`);
  }
}
