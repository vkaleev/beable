
import BasePage from 'src/pageObjects/base';

/**
 * Initial Beable page object
 */
class BeablePage extends BasePage {
  constructor() {
    super(`/`);
  }

  get getStartedBtn(): WebdriverIO.Element {
    return $(`button=GET STARTED`);
  }
}

export default new BeablePage();
