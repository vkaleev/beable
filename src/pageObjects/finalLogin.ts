
import BasePage from 'src/pageObjects/base';

/**
 * Final login page object
 */
class FinalLoginPage extends BasePage {
  constructor() {
    super(`/final-login`);
  }

  get usernameInput(): WebdriverIO.Element {
    return $(`#username`);
  }

  get passwordInput(): WebdriverIO.Element {
    return $(`#password`);
  }

  get submitBtn(): WebdriverIO.Element {
    return $(`button=LET'S GO!`);
  }

  /**
   * Fill final login form and authorize
   */
  public auth(username: string, password: string): void {
    this.usernameInput.setValue(username);
    this.passwordInput.setValue(password);
    this.submitBtn.click();
  }
}

export default new FinalLoginPage();
