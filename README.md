
# Tests for Beable web portal

## Setup
Install NodeJS >=10.15.1.

Install TypeScript >=3.6.4.

Run command `npm install`.

## How to use run tests
Run command `npm test`.

## Generate a report
Run command `npm run report`

## Required changes to the project
There is test sample login included in the project if you run it without changes.

To be able to test against Beable web portal please change `baseUrl` in [wdio.conf.js](wdio.conf.js) and remove 'skip' in test suite [ac1.spec.ts](test/ac1.spec.ts)

